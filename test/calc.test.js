import calc from '../src/calc.js';
import { assert, expect, should } from 'chai';

describe('Unit testing calc.js', () => {
    before(() => console.log("before test"));

    it('should add two numbers.', () => {
        expect(calc.add(1, 2)).to.equal(3);
        expect(calc.add(2, 2)).to.equal(4);
    });

    it('should subtract two numbers.', () => {
        assert(calc.subtract(1, 2) === -1);
        assert(calc.subtract(2, 1) === 1);
    });

    it('should multiply two numbers.', () => {
        should().equal(4, calc.multiply(2, 2));
        should().equal(8, calc.multiply(2, 4));      
    });

    it('should throw error in division with 0 as a divisor.', () => {
        expect(function(){
            calc.divide(1, 0);
        }).to.throw("Cannot devide with zero.");
    });

    after(() => console.log("after test"));
});