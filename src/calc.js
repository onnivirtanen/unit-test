
/**
 * Combines attends together
 * @param {number} a 
 * @param {number} b 
 * @returns {number} sum
 */
const add = (a, b) => a + b;

/**
 * Subtracts subtrahend from minuend
 * @param {number} a 
 * @param {number} b
 * @returns {number} difference
 */
const subtract = (a, b) => a - b;

/**
 * Multiplies multiplier and the multiplicand
 * @param {number} a 
 * @param {number} b 
 * @returns {number} product
 */
const multiply = (a, b) => a * b;

/**
 * Divides dividend by divisor
 * @param {number} a 
 * @param {number} b 
 * @returns quotient
 */
const divide = (a, b) => {
    if (b == 0) {
        throw "Cannot devide with zero.";
    }
    return a / b;
};

export default { add, subtract, multiply, divide }