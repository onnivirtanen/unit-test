import express from 'express';
import calc from './calc.js';
const app = express();
const port = 3000;
const host = "localhost";

app.get('/', (req, res) => {
    res.send("Hello world");
})

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.add(a, b));
});

app.get('/subtract', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.subtract(a, b));
})

app.get('/multiply', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.multiply(a, b));
})

app.get('/divide', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.divide(a, b));
})

app.listen(port, host, () => {
    console.log(`Server: http://localhost:${port}`);
})